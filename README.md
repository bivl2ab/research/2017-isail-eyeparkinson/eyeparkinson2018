Repositorio con los códigos asociados a la ponencia "Parkinsonian Ocular Fixation Patterns from Magnified Videos and CNN Features", presentada en el evento internacional CIARP2019.
Esta ponencia cuenta con memorias publicadas en la editorial Springer, como parte de la serie Lecture Notes in Computer Science, en el siguiente enlace:
https://link.springer.com/chapter/10.1007/978-3-030-33904-3_70