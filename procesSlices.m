%% Cropping for CNNs
%cd data2
cd results3_accel
clear all, clc

%listing = dir('*.avi');
listing = dir('*fm_15_alpha_15_pylevel_4_kernel_DOG.avi');
%%
L = length(listing);
for l = 1:L
    disp(l)
    load([listing(l).name(1:end-4),'.mat'])  
    S1_1 = S1(1:180,:,:);
    S1_2 = S1(121:end,:,:);
    S2_1 = S2(:,1:180,:);
    S2_2 = S2(:,121:end,:);
    S3_1 = S3(:,1:120,:);
    S3_2 = S3(:,181:end,:);
    S4_1 = S4(:,1:180,:);
    S4_2 = S4(:,121:end,:);
    save([listing(l).name(1:end-4),'_2sq.mat'],...
        'S1_1','S1_2','S2_1','S2_2','S3_1','S3_2','S4_1','S4_2')
end
%[]
cd ..



%% Originals Data Vectorization
%{
0 C1 
0 C2 
0 C3 
0 C4 
0 C5 
0 C6 
1 PK1 
1 PK2 
1 PK3 
1 PK4 
1 PK5 
1 PK6 
%}
    
clear all
sz1 = 300*180; % 1/3:100*60 | 1:300*180 
sz2 = 300*120; % 1/3:100*40 | 1:300*120 
frsz = 1;   % 1/3 | 1
prim = 'Sg'; % 'Sg' 'Gmag'
normaliz = 1;

cd data
listing = dir('*_S1.png');
L = length(listing);
S1 = zeros(L,sz1); 
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S'; S = S(:);
    S1(l,:) = S;
end
listing = dir('*_S2.png');
S2 = zeros(L,sz1); 
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S(:);
    S2(l,:) = S;
end
listing = dir('*_S3.png');
S3 = zeros(L,sz2); % <====
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S(:);
    S3(l,:) = S;
end
listing = dir('*_S4.png');
S4 = zeros(L,sz1); 
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S(:);
    S4(l,:) = S;
end
cd ..
if normaliz
    S1 = (S1-127.5)/127.5;
    S2 = (S2-127.5)/127.5;
    S3 = (S3-127.5)/127.5;
    S4 = (S4-127.5)/127.5;
end    
save('S_originals.mat','S1','S2','S3','S4','sz1','sz2')

%% Results Data Vectorization
addpath(genpath('results_accel'))
cd results_accel
param = 'fm_5.6_alpha_20_pylevel_4_kernel_DOG';

listing = dir(['**/*_',param,'_S1.png']);
L = length(listing);
S1 = zeros(L,sz1); 
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S'; S = S(:);
    S1(l,:) = S;
end
listing = dir(['**/*_',param,'_S2.png']);
S2 = zeros(L,sz1); 
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S(:);
    S2(l,:) = S;
end
listing = dir(['**/*_',param,'_S3.png']);
S3 = zeros(L,sz2); % <====
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S(:);
    S3(l,:) = S;
end
listing = dir(['**/*_',param,'_S4.png']);
S4 = zeros(L,sz1); 
for l = 1:L
    load(strcat(listing(l).name(1:end-4),'.mat'),prim)  
    S = imresize(eval(prim),frsz);
    S = S(:);
    S4(l,:) = S;
end
cd ..
if normaliz
    S1 = (S1-127.5)/127.5;
    S2 = (S2-127.5)/127.5;
    S3 = (S3-127.5)/127.5;
    S4 = (S4-127.5)/127.5;
end    
save('S_results.mat','S1','S2','S3','S4','sz1','sz2')

%% Data Partition
clear all
partition = 20;
data = {'S_originals.mat','S_results.mat'};
for d = 1:2
    load(data{d})
    S1p = cell(1,partition);
    block = sz1/partition;
    idx = 1;
    for p = 1:partition
       S1p{p} = S1(:,idx:block-1+idx);
       idx = idx + block;
    end    
    S2p = cell(1,partition);
    block = sz1/partition;
    idx = 1;
    for p = 1:partition
       S2p{p} = S2(:,idx:block-1+idx);
       idx = idx + block;
    end 
    S3p = cell(1,partition);
    block = sz2/partition; % <====
    idx = 1;
    for p = 1:partition
       S3p{p} = S3(:,idx:block-1+idx);
       idx = idx + block;
    end 
    S4p = cell(1,partition);
    block = sz1/partition;
    idx = 1;
    for p = 1:partition
       S4p{p} = S4(:,idx:block-1+idx);
       idx = idx + block;
    end 
    save(data{d},'S1p','S2p','S3p','S4p')
end    

%% Text Data format
clear all
data = {'S_originals.mat','S_results.mat'};
subjects = {'0 CT1','0 CT2','0 CT3','0 CT4','0 CT5','0 CT6',...
           '1 PK1','1 PK2','1 PK3','1 PK4','1 PK5','1 PK6'};
partitions = 20;
dataf = cell(12*partitions,2);
for d = 1:2
    load(data{d})
    c = 1;
    for s = 1:12
        for p = 1:partitions
            svec = [S1p{p}(s,:),S2p{p}(s,:),S3p{p}(s,:),S4p{p}(s,:)];
            id = [subjects{s},sprintf('.p%02d',p)];
            dataf{c,1} = id;
            dataf{c,2} = svec;
            c = c+1;
        end    
    end    
    file = fopen([data{d}(1:end-4),'.txt'],'w');
    for s = 1:c-1   
        disp(dataf{s,1})
        sString = '';
        for x = 1:length(dataf{1,2})
            xString = sprintf(' %d:%1.4f',x,dataf{s,2}(x));
            sString = [sString,xString];
        end 
        sString = [dataf{s,1},sString,'\n'];
        fprintf(file, sString);       
    end
    fclose(file);
end
%% Reorganize Lines  
clear all
partitions = 20;
data = {'S_originals.txt','S_results.txt'};
dataRO = {'S_originals_RO.txt','S_results_RO.txt'};
for d = 1:2
    file = fopen(data{d});
    lines = strings(12*partitions,1);
    linesRO = strings(12*partitions,1);
    for i = 1:12*partitions
        lines(i) = fgets(file);
    end
    fclose(file);
    idx = cell(12,1);
    c = 1;
    for s = 1:12
        for p = 1:partitions
            idx{s}(p) = c;
            c = c+1;
        end    
    end
    idx = idx([1,12,2,11,3,10,4,9,5,8,6,7]);
    c = 1;
    for s = 1:12
        for p = 1:partitions
            linesRO(c) = lines(idx{s}(p));
            c = c+1;            
        end    
    end 
    file = fopen(dataRO{d},'w');
    for i = 1:12*partitions
        lchar = char(linesRO(i));
        fprintf(file,[lchar(1),lchar(10:end)]);
    end 
    fclose(file);
end

%% K-fold Files
clear all
partitions = 20;
K = 4;
Klines = 12*partitions/K;
data = {'S_originals.txt','S_results.txt'};
dataRO = {'S_originals_RO.txt','S_results_RO.txt'};
folders = {'kfold_originals','kfold_results'};
mkdir txt
movefile(data{1},'txt');
movefile(data{2},'txt');
movefile(dataRO{1},'txt');
movefile(dataRO{2},'txt');
movefile S_originals.mat txt
movefile S_results.mat txt
cd txt
mkdir kfold_originals
mkdir kfold_results
for d = 1:2
    file = fopen(dataRO{d});
    lines = strings(12*partitions,1);
    for i = 1:12*partitions
        lines(i) = fgets(file);
    end
    fclose(file);    
    cd(folders{d})
    c = 0;
    for k = 1:K
        file = fopen(sprintf('test%02d.txt',k),'w');
        ilinesTest = 1+c : Klines+c; 
        for i = ilinesTest
            fprintf(file, lines(i));
        end
        fclose(file);

        file = fopen(sprintf('train%02d.txt',k),'w');
        ilinesTrain = setdiff(1:12*partitions,ilinesTest);
        for i = ilinesTrain
            fprintf(file, lines(i));
        end    
        fclose(file);

        c = c + Klines;
    end  
    cd ..     
end
cd ..

%%     
        
    












