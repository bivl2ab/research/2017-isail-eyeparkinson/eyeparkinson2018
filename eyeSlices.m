%{
I = imread('r.png');
for i = 1:100
    F(i).data = I;
end
I = imread('g.png');
for i = 101:200
    F(i).data = I;
end
I = imread('b.png');
for i = 201:300
    F(i).data = I;
end

St(:,:,1) = S(:,:,1)'; 
St(:,:,2) = S(:,:,2)';
St(:,:,3) = S(:,:,3)';
imwrite(St,strcat(listing(l).name(1:end-4),'_S1t.png'))
Sf(:,:,1) = flip(S(:,:,1)); 
Sf(:,:,2) = flip(S(:,:,2));
Sf(:,:,3) = flip(S(:,:,3));
imwrite(Sf,strcat(listing(l).name(1:end-4),'_S1f.png'))
%}

%%
clear all, clc

cd results3_accel\
%cd data2\
listing = dir('*fm_15_alpha_15_pylevel_4_kernel_DOG.avi');

%%
for l = 1:length(listing)
    disp(l)
    
    vidObj = VideoReader(listing(l).name);
    k = 1;
    while hasFrame(vidObj)
        F(k).data = readFrame(vidObj);
        k = k+1;
    end
    F(300).data = F(299).data; 
    k = k+1;
    %% S1: XT-Slice
    yPixel = 60;
    S = zeros(size(F(1).data,2),3,k-1,'uint8');
    for i = 1:k-1
       Fi = F(i).data;
       S(:,:,i) = squeeze(Fi(yPixel,:,:));   
    end
    S = permute(S,[3 1 2]);
    imwrite(S,strcat(listing(l).name(1:end-4),'_S1.png')) 
    S1 = S;
    %% S2: 45�-Slice
    P1 = [1 120]; P2 = [180 1]; 
    x1 = P1(1); y1 = -P1(2);
    x2 = P2(1); y2 = -P2(2);
    x = x1:x2;
    y = round(((y2-y1)/(x2-x1))*(x - x1) + y1);  
    xRange = x2 - x1 + 1;
    S = zeros(xRange,3,k-1,'uint8');
    for i = 1:k-1
       Fi = F(i).data;
       for j = 1:length(x)
           S(j,:,i) = Fi(-y(j),x(j),:);
       end
    end   
    S = permute(S,[1 3 2]);
    if (y2-y1)/(x2-x1) > 0
        S = flip(S);  
    end
    imwrite(S,strcat(listing(l).name(1:end-4),'_S2.png')) 
    S2 = S;
    %% S3: YT-Slice
    xPixel = 90;
    S = zeros(size(F(1).data,1),3,k-1,'uint8');
    for i = 1:k-1
        Fi = F(i).data;
        S(:,:,i) = squeeze(Fi(:,xPixel,:));   
    end
    S = permute(S,[1 3 2]);
    imwrite(S,strcat(listing(l).name(1:end-4),'_S3.png'))
    S3 = S;
   %% S4: 135�-Slice
    P1 = [1 1]; P2 = [180 120]; 
    x1 = P1(1); y1 = -P1(2);
    x2 = P2(1); y2 = -P2(2);
    x = x1:x2;
    y = round(((y2-y1)/(x2-x1))*(x - x1) + y1);
    xRange = x2 - x1 + 1;
    S = zeros(xRange,3,k-1,'uint8');
    for i = 1:k-1
       Fi = F(i).data;
       for j = 1:length(x)
           S(j,:,i) = Fi(-y(j),x(j),:);
       end
    end   
    S = permute(S,[1 3 2]);
    if (y2-y1)/(x2-x1) > 0
        S = flip(S);  
    end
    imwrite(S,strcat(listing(l).name(1:end-4),'_S4.png'))  
    S4 = S;
    %%
    save([listing(l).name(1:end-4),'.mat'],'S1','S2','S3','S4') 
end
cd ..
%%   









