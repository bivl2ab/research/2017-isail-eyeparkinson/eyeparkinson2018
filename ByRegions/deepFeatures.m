clear all, clc
%{}
%[]
cd data2
listing = dir('*.avi');
savevar = 'data2_deepFeatures.mat';

cd results2_accel
listing = dir('*_5_pylevel_4_kernel_DOG.avi');
savevar = 'results2_deepFeatures_5.mat';
%%
%L = length(listing);
net1 = vgg19;
net2 = resnet101;
net3 = inceptionresnetv2;
iSz1 = [224,224]; iSz2 = [299,299];
slicesCrop = {'S1_1','S1_2','S2_1','S2_2','S3_1','S3_2','S4_1','S4_2'};
mag = 15;
%% Computing Deep Features
for freq = 11:15
    cd results3_accel
    disp(sprintf('Frequency: %d',freq))
    listing = dir(sprintf('*%d_alpha_%d_pylevel_4_kernel_DOG.avi',freq,mag));
    savevar = sprintf('results3_deepFeatures_%d_f%d.mat',mag,freq);
    L = length(listing);
    for l = 1:L
        disp(l)
        load([listing(l).name(1:end-4),'_2sq.mat'])  
        for s = 1:8
            eval(['V(l).',slicesCrop{s},'.imgSz1 = imresize(',slicesCrop{s},',iSz1);'])
            eval(['V(l).',slicesCrop{s},'.net1_layerA = activations(net1,V(l).',slicesCrop{s},'.imgSz1,''pool1'');'])
            eval(['V(l).',slicesCrop{s},'.net1_layerB = activations(net1,V(l).',slicesCrop{s},'.imgSz1,''pool2'');'])
            eval(['V(l).',slicesCrop{s},'.net2_layerA = activations(net2,V(l).',slicesCrop{s},'.imgSz1,''conv1_relu'');'])
            eval(['V(l).',slicesCrop{s},'.net2_layerB = activations(net2,V(l).',slicesCrop{s},'.imgSz1,''res2a_relu'');'])
            eval(['V(l).',slicesCrop{s},'.imgSz2 = imresize(',slicesCrop{s},',iSz2);'])
            eval(['V(l).',slicesCrop{s},'.net3_layerA = activations(net3,V(l).',slicesCrop{s},'.imgSz2,''activation_3'');'])
            eval(['V(l).',slicesCrop{s},'.net3_layerB = activations(net3,V(l).',slicesCrop{s},'.imgSz2,''mixed_5b'');'])
        end  
    end
    cd ..
    pause(0.1)
    save(savevar,'V')
    pause(0.1)
end

%% Checking ranges
figure(1)
for ch = 1:64
    CH = V(1).S4_2.net1_layerA(:,:,ch);
    strng = sprintf('%d: %1.4f - %1.4f', ch, min(CH(:)), max(CH(:)));
    disp(strng)
    plot(CH(:),'.')
    pause
end


%%
load data2_deepFeatures.mat
load results2_deepFeatures.mat
slicePair = 'S4';
%% For Si_1 and Si_2, net1_layerA
featureM1 = zeros(12,112*112*64);
featureM2 = zeros(12,112*112*64);
for l = 1:12
    eval(['featureM1(l,:) = V(l).',slicePair,'_1.net1_layerA(:);'])
    eval(['featureM2(l,:) = V(l).',slicePair,'_2.net1_layerA(:);']) 
end
%{
% featureM1     featureM2
% C1 Si_1       C1 Si_2
% C2 Si_1       C2 Si_2
% C3 Si_1       C3 Si_2
% C4 Si_1       C4 Si_2
% C5 Si_1       C5 Si_2
% C6 Si_1       C6 Si_2
% PK1 Si_1      PK1 Si_2
% PK2 Si_1      PK2 Si_2
% PK3 Si_1      PK3 Si_2
% PK4 Si_1      PK4 Si_2
% PK5 Si_1      PK5 Si_2
% PK6 Si_1      PK6 Si_2
%}
AllSet = zeros(24,112*112*64);
for m = 1:2:24
    AllSet(m,:) = featureM1(ceil(m/2),:);
    AllSet(m+1,:) = featureM2(ceil(m/2),:);
end
%{
% C1 Si_1       
% C1 Si_2
% C2 Si_1
% C2 Si_2
% C3 Si_1      
% C3 Si_2
% C4 Si_1 
% C4 Si_2
% C5 Si_1  
% C5 Si_2
% C6 Si_1      
% C6 Si_2
% PK1 Si_1      
% PK1 Si_2
% PK2 Si_1     
% PK2 Si_2
% PK3 Si_1     
% PK3 Si_2
% PK4 Si_1     
% PK4 Si_2
% PK5 Si_1     
% PK5 Si_2
% PK6 Si_1     
% PK6 Si_2
%}
AllSetRO = zeros(24,112*112*64);
idx = cell(12,1);
c = 1;
for s = 1:12
    for p = 1:2
        idx{s}(p) = c;
        c = c+1;
    end    
end
idx = idx([1,12,2,11,3,10,4,9,5,8,6,7]);
c = 1;
for s = 1:12
    for p = 1:2
        AllSetRO(c,:) = AllSet(idx{s}(p),:);
        c = c+1;            
    end    
end 
%{
% C1 Si_1       
% C1 Si_2
% PK6 Si_1     
% PK6 Si_2
% C2 Si_1
% C2 Si_2
% PK5 Si_1     
% PK5 Si_2
% C3 Si_1      
% C3 Si_2
% PK4 Si_1     
% PK4 Si_2
% C4 Si_1 
% C4 Si_2
% PK3 Si_1     
% PK3 Si_2
% C5 Si_1  
% C5 Si_2
% PK2 Si_1     
% PK2 Si_2
% C6 Si_1      
% C6 Si_2
% PK1 Si_1      
% PK1 Si_2
%}
K = 6;
Klines = 24/K;
c = 0;
for k = 1:K
    eval(['Test.k',num2str(k),' = zeros(',num2str(Klines),',112*112*64);'])
    ilinesTest = 1+c : Klines+c;    
    eval(['Test.k',num2str(k),' = AllSetRO(ilinesTest,:);'])
    %   
    eval(['Train.k',num2str(k),' = zeros(',num2str(24-Klines),',112*112*64);'])
    ilinesTrain = setdiff(1:24,ilinesTest);
    eval(['Train.k',num2str(k),' = AllSetRO(ilinesTrain,:);'])
    c = c + Klines;
end      
%========================================
for k = 1:K
    disp(k)
    eval(['[coeff,scoreTrain,~,~,explained,mu] = pca(Train.k',num2str(k),');'])
    eval(['scoreTrain95.k',num2str(k),' = scoreTrain;'])
    eval(['scoreTest95.k',num2str(k),' = (Test.k',num2str(k),' - mu)*coeff;'])
end    

eval([slicePair,'.scoreTrain95 = scoreTrain95;'])
eval([slicePair,'.scoreTest95 = scoreTest95;'])

%% Saving
data2.S1 = S1;
data2.S2 = S2;
data2.S3 = S3;
data2.S4 = S4;
results2.S1 = S1;
results2.S2 = S2;
results2.S3 = S3;
results2.S4 = S4;
save('pcaKfold.mat','data2','results2') 
clear all
%%
dr = {'data2','results2'};
idr = 2;
labels = {'0','0','1','1'};     
%{
% C1 Si_1       
% C1 Si_2
% PK6 Si_1     
% PK6 Si_2
% C2 Si_1
% C2 Si_2
% PK5 Si_1     
% PK5 Si_2
% C3 Si_1      
% C3 Si_2
% PK4 Si_1     
% PK4 Si_2
% C4 Si_1 
% C4 Si_2
% PK3 Si_1     
% PK3 Si_2
% C5 Si_1  
% C5 Si_2
% PK2 Si_1     
% PK2 Si_2
% C6 Si_1      
% C6 Si_2
% PK1 Si_1      
% PK1 Si_2
%}
K = 6;
Klines = 24/K;         
for k = 1:K
    eval(['concatS_Ktest = [',dr{idr},'.S1.scoreTest95.k',num2str(k),...
                           ',',dr{idr},'.S2.scoreTest95.k',num2str(k),...
                           ',',dr{idr},'.S3.scoreTest95.k',num2str(k),...
                           ',',dr{idr},'.S4.scoreTest95.k',num2str(k),'];'])    
    eval(['concatS_Ktrain = [',dr{idr},'.S1.scoreTrain95.k',num2str(k),...
                           ',',dr{idr},'.S2.scoreTrain95.k',num2str(k),...
                           ',',dr{idr},'.S3.scoreTrain95.k',num2str(k),...
                           ',',dr{idr},'.S4.scoreTrain95.k',num2str(k),'];'])
    %----------------------------------------------------------------------
    file = fopen(sprintf('test%02d.txt',k),'w');
    for s = 1:Klines  
        sString = '';
        for x = 1:76
            xString = sprintf(' %d:%1.4f',x,concatS_Ktest(s,x));
            sString = [sString,xString];
        end 
        sString = [labels{s},sString,'\n'];
        fprintf(file, sString);       
    end
    fclose(file);
    %----------------------------------------------------------------------
    file = fopen(sprintf('train%02d.txt',k),'w');
    c = 0; 
    for s = 1:24-Klines 
        sString = '';
        for x = 1:76
            xString = sprintf(' %d:%1.4f',x,concatS_Ktrain(s,x));
            sString = [sString,xString];
        end 
        sString = [labels{s-4*c},sString,'\n'];
        fprintf(file, sString);
        if mod(s,4)==0, c = c+1; end
    end
    fclose(file);         
end



%%
%file = fopen('concatS_scoreTrain95.txt','w');
file = fopen('concatS_scoreTest95.txt','w');
labels = {'0','1','0','1','0','1','0','1','0','1','0','1'};
for s = 1:12  
    disp(s)
    sString = '';
    for x = 1:40
        %xString = sprintf(' %d:%1.4f',x,concatS_scoreTrain95(s,x));
        xString = sprintf(' %d:%1.4f',x,concatS_scoreTest95(s,x));
        sString = [sString,xString];
    end 
    sString = [labels{s},sString,'\n'];
    fprintf(file, sString);       
end
fclose(file);
                    
                   
%%
[coeff,scoreTrain,~,~,explained,mu] = pca(Train);
sum(explained(1:10))
scoreTrain95 = scoreTrain(:,1:10);
scoreTest95 = (Test - mu)*coeff(:,1:10);
                    
                    
%%
load USF17Gal %each sample is a third-order tensor of size 32x22x10
N=ndims(fea3D)-1;%Order of the tensor sample
Is=size(fea3D);%32x22x10x731
numSpl=Is(4);%There are 731 gait samples
testQ=97;%Keep 97% variation in each mode
maxK=1;%One iteration only
[tUs, odrIdx, TXmean, Wgt] = MPCA(fea3D,gnd,testQ,maxK);
fea3Dctr=fea3D-repmat(TXmean,[ones(1,N), numSpl]);%Centering
newfea = ttm(tensor(fea3Dctr),tUs,1:N);%MPCA projection
%Vectorization of the tensorial feature
newfeaDim=size(newfea,1)*size(newfea,2)*size(newfea,3);
newfea=reshape(newfea.data,newfeaDim,numSpl)';%Note: Transposed
P=1000;
selfea=newfea(:,odrIdx(1:P));%Select the first "P" sorted features
%"P" is the dimension of the final feature vector to be fed into a 
%standard classifier (e.g., nearest neighbor classifier), you may 
%need to test different values of P for best performance
Wgt=reshape(Wgt,newfeaDim,1);%Vectorizing weight tensor
Wgt=Wgt(odrIdx);%Select the weights accordingly



%%
net = vgg19;
inputSize = net.Layers(1).InputSize;
analyzeNetwork(net2)
features = activations(net,V(l).S1_1,'pool1');
figure,imshow(features(:,:,3),[])
rfeatures = reshape(features,[112 112 1 64]);
grid = imtile(mat2gray(rfeatures),'GridSize',[8 8]);
imshow(grid,[])

%%
figure,

subplot(2,3,1)
features = V(7).S3_1.net1_layerA;
rfeatures = reshape(features,[112 112 1 64]);
sfeatures = rfeatures(:,:,:,randperm(64,12));
grid = imtile(mat2gray(sfeatures),'GridSize',[3 4]);
imshow(grid,[])
subplot(2,3,4)
features = V(7).S3_1.net1_layerB;
rfeatures = reshape(features,[56 56 1 128]);
sfeatures = rfeatures(:,:,:,randperm(128,12));
grid = imtile(mat2gray(sfeatures),'GridSize',[3 4]);
imshow(grid,[])

subplot(2,3,2)
features = V(7).S3_1.net2_layerA;
rfeatures = reshape(features,[112 112 1 64]);
sfeatures = rfeatures(:,:,:,randperm(64,12));
grid = imtile(mat2gray(sfeatures),'GridSize',[3 4]);
imshow(grid,[])
subplot(2,3,5)
features = V(7).S3_1.net2_layerB;
rfeatures = reshape(features,[56 56 1 256]);
sfeatures = rfeatures(:,:,:,randperm(256,12));
grid = imtile(mat2gray(sfeatures),'GridSize',[3 4]);
imshow(grid,[])

subplot(2,3,3)
features = V(7).S3_1.net3_layerA;
rfeatures = reshape(features,[147 147 1 64]);
sfeatures = rfeatures(:,:,:,randperm(64,12));
grid = imtile(mat2gray(sfeatures),'GridSize',[3 4]);
imshow(grid,[])
subplot(2,3,6)
features = V(7).S3_1.net3_layerB;
rfeatures = reshape(features,[35 35 1 320]);
sfeatures = rfeatures(:,:,:,randperm(320,12));
grid = imtile(mat2gray(sfeatures),'GridSize',[3 4]);
imshow(grid,[])


%%
%     V(l).S1_1.imgSz1 = imresize(S1_1,iSz1);
%     V(l).S1_1.net1_pool1 = activations(net1,V(l).S1_1.img,'pool1');
%     V(l).S1_1.net1_pool2 = activations(net1,V(l).S1_1.img,'pool2');
%     V(l).S1_1.net2_pool1 = activations(net2,V(l).S1_1.img,'conv1_relu');
%     V(l).S1_1.net2_pool2 = activations(net2,V(l).S1_1.img,'res2a_relu');
%     V(l).S1_1.imgSz2 = imresize(S1_1,iSz2);
%     V(l).S1_1.net3_pool1 = activations(net3,V(l).S1_1.imgSz2,'activation_3');
%     V(l).S1_1.net3_pool2 = activations(net3,V(l).S1_1.imgSz2,'mixed_5b');
%     
%     eval('V(l).S1_1.imgSz1 = imresize(S1_1,iSz1);')
%     eval("V(l).S1_1.net1_pool1 = activations(net1,V(l).S1_1.img,'pool1')")



