%% data2 / results2
clear all, clc
load data2_deepFeatures.mat, dr = 'data2';
load results2_deepFeatures_5.mat, dr = 'results2';
%% Layers & Slices Pairs
dr = 'results3';
layers = {'net1_layerA',...   %1
          'net1_layerB',...   %2
          'net2_layerA',...   %3
          'net2_layerB',...   %4
          'net3_layerA',...   %5
          'net3_layerB'};     %6
PC = [10, 50, 10, 30, 30, 40];      
slicePair = {'S1_1','S1_2';...
             'S2_1','S2_2';...
             'S3_1','S3_2';...
             'S4_1','S4_2'};
mag = 15;
%% For Si_1 and Si_2, net#_layer?
for freq = 11:15
    load(sprintf('results3_deepFeatures_%d_f%d.mat',mag,freq))
    pause(0.1)
    mkdir(sprintf('features_%d_f%d',mag,freq))
    for ly = 1:6
        szCov = eval(['size(V(1).S1_1.',layers{ly},',3);' ]);
        szCovPCA = PC(ly);
        for sN = 1:4 %1:4 
            strng = sprintf('Layer %d - Slice %d',ly,sN);
            disp(strng)             
            CovM1 = zeros(12,szCov,szCov);
            CovM2 = zeros(12,szCov,szCov);
            CovM1_PCA = zeros(12,szCovPCA,szCovPCA);
            CovM2_PCA = zeros(12,szCovPCA,szCovPCA);  
            CovM1_PCAE = zeros(12,szCovPCA,szCovPCA);
            CovM2_PCAE = zeros(12,szCovPCA,szCovPCA);          
            for sP = 1:2 %1:2
                for l = 1:12 
                    eval(['Do = double(V(l).',slicePair{sN,sP},'.',layers{ly},');'])
                    Do = reshape(Do,[],szCov); muDo = mean(Do,1);
                    D = Do - repmat(muDo,size(Do,1),1);
                    eval(sprintf('CovM%d(l,:,:) = (D''*D)/size(Do,1);',sP)) 
                    eval(sprintf('[Wk,Ak{l}] = eigs(squeeze(CovM%d(l,:,:)),szCovPCA);',sP))
                    eval(sprintf('CovM%d_PCA(l,:,:) = Wk''*squeeze(CovM%d(l,:,:))*Wk;',sP,sP))
                    eval(sprintf('[Wpca,Apca{l}] = eig(squeeze(CovM%d_PCA(l,:,:)));',sP))
                    eval(sprintf('CovM%d_PCAE(l,:,:) = Wpca*logm(Apca{l})*Wpca'';',sP))           
                end
            end
            %{
            %%%%%%%%%%%%%% DEBUGGING
            eval([layers{ly},'.',slicePair{sN,1},'=','{Ak,Apca,CovM1,CovM1_PCA};'])
            %}
            %
            %# Descriptor
            descriptorM1 = zeros(12,0.5*(szCovPCA+szCovPCA^2));
            descriptorM2 = zeros(12,0.5*(szCovPCA+szCovPCA^2));
            for l = 1:12
               Tri = tril(ones(szCovPCA,szCovPCA));
               Tri(Tri==0) = NaN;
               covTri = Tri.*squeeze(CovM1_PCAE(l,:,:));
               descriptorM1(l,:) = rmmissing(covTri(:));
               covTri = Tri.*squeeze(CovM2_PCAE(l,:,:));
               descriptorM2(l,:) = rmmissing(covTri(:)); 
            end    
            %{
            % descriptorM1  descriptorM2
            % C1 Si_1       C1 Si_2
            % C2 Si_1       C2 Si_2
            % C3 Si_1       C3 Si_2
            % C4 Si_1       C4 Si_2
            % C5 Si_1       C5 Si_2
            % C6 Si_1       C6 Si_2
            % PK1 Si_1      PK1 Si_2
            % PK2 Si_1      PK2 Si_2
            % PK3 Si_1      PK3 Si_2
            % PK4 Si_1      PK4 Si_2
            % PK5 Si_1      PK5 Si_2
            % PK6 Si_1      PK6 Si_2
            %}
            AllSet = zeros(24,0.5*(szCovPCA+szCovPCA^2));
            for m = 1:2:24
                AllSet(m,:) = descriptorM1(ceil(m/2),:);
                AllSet(m+1,:) = descriptorM2(ceil(m/2),:);
            end
            %{
            % C1 Si_1       
            % C1 Si_2
            % C2 Si_1
            % C2 Si_2
            % C3 Si_1      
            % C3 Si_2
            % C4 Si_1 
            % C4 Si_2
            % C5 Si_1  
            % C5 Si_2
            % C6 Si_1      
            % C6 Si_2
            % PK1 Si_1      
            % PK1 Si_2
            % PK2 Si_1     
            % PK2 Si_2
            % PK3 Si_1     
            % PK3 Si_2
            % PK4 Si_1     
            % PK4 Si_2
            % PK5 Si_1     
            % PK5 Si_2
            % PK6 Si_1     
            % PK6 Si_2
            %}
            AllSetRO = zeros(24,0.5*(szCovPCA+szCovPCA^2));
            idx = cell(12,1);
            c = 1;
            for s = 1:12
                for p = 1:2
                    idx{s}(p) = c;
                    c = c+1;
                end    
            end
            idx = idx([1,12,2,11,3,10,4,9,5,8,6,7]);
            c = 1;
            for s = 1:12
                for p = 1:2
                    AllSetRO(c,:) = AllSet(idx{s}(p),:);
                    c = c+1;            
                end    
            end 
            %{
            % C1 Si_1       
            % C1 Si_2
            % PK6 Si_1     
            % PK6 Si_2
            % C2 Si_1
            % C2 Si_2
            % PK5 Si_1     
            % PK5 Si_2
            % C3 Si_1      
            % C3 Si_2
            % PK4 Si_1     
            % PK4 Si_2
            % C4 Si_1 
            % C4 Si_2
            % PK3 Si_1     
            % PK3 Si_2
            % C5 Si_1  
            % C5 Si_2
            % PK2 Si_1     
            % PK2 Si_2
            % C6 Si_1      
            % C6 Si_2
            % PK1 Si_1      
            % PK1 Si_2
            %}
            K = 12;
            Klines = 24/K;
            c = 0;
            for k = 1:K
                eval(['Test.k',num2str(k),' = zeros(',num2str(Klines),',',num2str(0.5*(szCovPCA+szCovPCA^2)),');'])
                ilinesTest = 1+c : Klines+c;    
                eval(['Test.k',num2str(k),' = AllSetRO(ilinesTest,:);'])
                %   
                eval(['Train.k',num2str(k),' = zeros(',num2str(24-Klines),',',num2str(0.5*(szCovPCA+szCovPCA^2)),');'])
                ilinesTrain = setdiff(1:24,ilinesTest);
                eval(['Train.k',num2str(k),' = AllSetRO(ilinesTrain,:);'])
                c = c + Klines;
            end   
            eval([slicePair{sN,1}(1:2),'.Train = Train;'])
            eval([slicePair{sN,1}(1:2),'.Test = Test;'])
            %} 
        clearvars -except mag freq V PC layers ly dr slicePair ...
        szCov szCovPCA S1 S2 S3 S4 ...
        net1_layerA net1_layerB net2_layerA net2_layerB net3_layerA net3_layerB        
        end
        eval([dr,'.S1 = S1;'])
        eval([dr,'.S2 = S2;'])
        eval([dr,'.S3 = S3;'])
        eval([dr,'.S4 = S4;'])
        save([dr,'_',layers{ly}],dr)
        disp(['Ok. Moving ',dr,'_',layers{ly},'.mat',' to ',sprintf('features_%d_f%d',mag,freq)])
        movefile([dr,'_',layers{ly},'.mat'],sprintf('features_%d_f%d',mag,freq))
        clearvars -except mag freq V PC layers ly dr slicePair 
    end
%{
%%%%%%%%%%%%%% DEBUGGING
save(['CovEigen_',dr],'net1_layerA','net1_layerB',...
                      'net2_layerA','net2_layerB',...
                      'net3_layerA','net3_layerB');
%}    
    clear V
end

%% Plotting Eigens 
%{
% Covariances
figure
for sj = 1:12
    Cov = net3_layerB.S1_1{3}(sj,:,:);
    strng = sprintf('%d: %1.4f - %1.4f', sj, min(Cov(:)), max(Cov(:)));
    disp(strng)
    plot(Cov(:),'.')
    pause
end
% Eigenvectors
figure
for sj = 1:12
    eV = net1_layerA.S1_1{1}{sj};
    strng = sprintf('%d: %1.4f - %1.4f', sj, min(eV(:)), max(eV(:)));
    disp(strng)
    plot(eV(:),'.')
    pause
end
% Eigenvalues
figure
subplot(1,6,1), hold on, title('VGG-19 Layer A')
for sj = 1:12
    plot(flip(diag(net1_layerA.S1_1{2}{sj})),'.')
end
subplot(1,6,2), hold on, title('VGG-19 Layer B')
for sj = 1:12
    plot(flip(diag(net1_layerB.S1_1{2}{sj})),'.')
end
subplot(1,6,3), hold on, title('ResNet-101 Layer A')
for sj = 1:12
    plot(flip(diag(net2_layerA.S1_1{2}{sj})),'.')
end
subplot(1,6,4), hold on, title('ResNet-101 Layer B')
for sj = 1:12
    plot(flip(diag(net2_layerB.S1_1{2}{sj})),'.')
end
subplot(1,6,5), hold on, title('InceptResNet-v2 Layer A')
for sj = 1:12
    plot(flip(diag(net3_layerA.S1_1{2}{sj})),'.')
end
subplot(1,6,6), hold on, title('InceptResNet-v2 Layer B')
for sj = 1:12
    plot(flip(diag(net3_layerB.S1_1{2}{sj})),'.')
end
% Cummulative Eigenvalues
figure
hold on
for sj = 1:12
    egValSum = cumsum(diag(net3_layerB.S1_1{2}{sj})); 
    plot(egValSum,'.')
    TSum = egValSum(end); TSumP = 0.05*TSum;
    c = 0;
    for i = 1:numel(egValSum)
        if (egValSum(i) >= (TSum - TSumP)) && (c == 0)
            PC(sj) = i;
            disp(i), c = 1;
        end
    end
end
mean(PC)

%                           DATA S1_1   DATA S4_1   RESULTS S1_1    RESULTS S4_1
% net1_layerA => PC: 10     !9.333      9.250           10.33       10.75
% net1_layerB => PC: 50     !50.58      49.17           54.92       52.67
% net2_layerA => PC: 10     !7.250      7.250           8.417       7.750
% net2_layerB => PC: 30     !23.58      25.17           27.50       25.92
% net3_layerA => PC: 30     !23.83      25.33           27.67       23.92
% net3_layerB => PC: 40     !38.00      32.92           34.42       39.58

%}
%% Generating txt's
clear all, clc
layers = {'net1_layerA',...   %1
          'net1_layerB',...   %2
          'net2_layerA',...   %3
          'net2_layerB',...   %4
          'net3_layerA',...   %5
          'net3_layerB'};     %6
dr = {'data2','results3'};
K = 12;
Klines = 24/K; 
labels = {'00','11','00','11',...
          '00','11','00','11',...
          '00','11','00','11'}; 
mag = 15;
%NumSlices = 1;      
%{
% C1 Si_1       
% C1 Si_2
% PK6 Si_1     
% PK6 Si_2
% C2 Si_1
% C2 Si_2
% PK5 Si_1     
% PK5 Si_2
% C3 Si_1      
% C3 Si_2
% PK4 Si_1     
% PK4 Si_2
% C4 Si_1 
% C4 Si_2
% PK3 Si_1     
% PK3 Si_2
% C5 Si_1  
% C5 Si_2
% PK2 Si_1     
% PK2 Si_2
% C6 Si_1      
% C6 Si_2
% PK1 Si_1      
% PK1 Si_2
%}
%%
for freq = 11:15
    disp(sprintf('=> Creating features_%d_f%d',mag,freq))
    cd(sprintf('features_%d_f%d',mag,freq))
    mkdir('S1')
    mkdir('S2')
    mkdir('S4')
    for NumSlices = [1 2 4]
        for ly = 1:6
            for idr = 2:2 %%%%%%% 1:2
                load([dr{idr},'_',layers{ly},'.mat'])
                mkdir([dr{idr},'_',layers{ly}])
                cd([dr{idr},'_',layers{ly}])
                for k = 1:K
                if NumSlices == 1, eval(['concatS_Ktest = [',dr{idr},'.S2.Test.k',num2str(k),'];']), end
                if NumSlices == 2, eval(['concatS_Ktest = [',dr{idr},'.S1.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S3.Test.k',num2str(k),'];']), end
                if NumSlices == 4, eval(['concatS_Ktest = [',dr{idr},'.S1.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S2.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S3.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S4.Test.k',num2str(k),'];']), end                            
                if NumSlices == 1, eval(['concatS_Ktrain = [',dr{idr},'.S2.Train.k',num2str(k),'];']), end
                if NumSlices == 2, eval(['concatS_Ktrain = [',dr{idr},'.S1.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S3.Train.k',num2str(k),'];']), end   
                if NumSlices == 4, eval(['concatS_Ktrain = [',dr{idr},'.S1.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S2.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S3.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S4.Train.k',num2str(k),'];']), end
                     Ktest{k} = concatS_Ktest;
                     Ktrain{k} = concatS_Ktrain;
                end
                clear data2 results2 concatS_Ktest concatS_Ktrain
                for k = 1:K
                    %disp(k)
                   %----------------------------------------------------------------------
                    file = fopen(sprintf('test%02d.txt',k),'w');
                    for s = 1:Klines  
                        sString = '';
                        for x = 1:size(Ktest{k},2)
                            xString = sprintf(' %d:%1.4f',x,Ktest{k}(s,x));
                            sString = [sString,xString];
                        end 
                        sString = [labels{k}(s),sString,'\n'];
                        fprintf(file, sString);       
                    end
                    fclose(file);
                    %----------------------------------------------------------------------
                    file = fopen(sprintf('train%02d.txt',k),'w');
                    idxlb = setdiff(1:K,k); Tlabels = strcat(labels{idxlb});
                    for s = 1:24-Klines 
                        sString = '';
                        for x = 1:size(Ktrain{k},2)
                            xString = sprintf(' %d:%1.4f',x,Ktrain{k}(s,x));
                            sString = [sString,xString];
                        end 
                        sString = [Tlabels(s),sString,'\n'];
                        fprintf(file, sString);
                    end
                    fclose(file);         
                end
                clearvars -except mag freq layers ly dr idr K Klines labels NumSlices
                cd ..
                disp(['Ok. Moving ',dr{idr},'_',layers{ly},' to ','S',num2str(NumSlices)])
                movefile([dr{idr},'_',layers{ly}],['S',num2str(NumSlices)])
            end
        end
    end
    cd ..
end
%% Extract Accuracies
clear all, clc
mag = 15;
for freq = 11:15
    cd(sprintf('features_%d_f%d',mag,freq))
    disp(sprintf('####### DISPLAYING features_%d_f%d #############',mag,freq))   
    for NumSlices = [1 2 4]
        cd(['S',num2str(NumSlices)])
        %disp(['%%%%%%%%%% ','S',num2str(NumSlices),' %%%%%%%%%%']) 
        listing = dir();
        for dr = 3:8
           cd(listing(dr).name)
           %disp(listing(dr).name)
           for k = 1:12
               file = fopen(sprintf('k%d.txt',k));
               line = fgetl(file);
               %disp(['k',num2str(k),': ',line])
               numbers = regexp(line,'\d*','Match');
               Acc(k) = str2double(numbers{1});
               fclose(file);
           end
           %disp(['MEAN ACC. => ',num2str(mean(Acc),4)]) 
           disp(num2str(mean(Acc),4)) 
           %disp(' ')
           cd ..
        end
        cd ..
    end
    cd ..
end

%% LibSVM
cd libsvm-3.23w\tools
tic
parfor k = 1:12
   disp(sprintf('Starting k%02d',k))
   cmd0 = 'python easy.py ';
   cmd1 = ['..\..\results2_features\exp2\data2_net1_layerA\',sprintf('train%02d.txt ',k)];
   cmd2 = ['..\..\results2_features\exp2\data2_net1_layerA\',sprintf('test%02d.txt ',k)];
   cmd3 = ['> ..\..\results2_features\exp2\data2_net1_layerA\',sprintf('k%02d.txt',k)]; 
   system([cmd0,cmd1,cmd2,cmd3]); 
   disp(sprintf('Ok k%02d',k))
end
toc
cd ..\..

%Elapsed time is 75.112836 seconds.




%%
cd libsvm-3.23w\tools
python easy.py ..\..\results2_features\data2_net1_layerA\train01.txt ..\..\results2_features\data2_net1_layerA\test01.txt > ..\..\results2_features\data2_net1_layerA\k1.txt
cd ..\..

imshow(squeeze(CovM1(12,:,:)),[])
imshow(squeeze(CovM2(1,:,:)),[])

plotCov = permute(CovM1,[2 3 1]);
grid = imtile(mat2gray(plotCov),'GridSize',[3 4]);
imshow(grid,[])
imshow(squeeze(CovM1(1,:,:)))
colormap hsv
colormap jet

A = V(1).S1_1.net1_layerA(:,:,1);
B = V(1).S1_1.net1_layerA(:,:,2);
Cov = cov(A,B); Cov = Cov(2);

