%%
clear all, clc

addpath(genpath('data2'))
cd data2\
listing = dir('*.avi');
cd ..

addpath(genpath('results2_accel'))
cd results2_accel\
listing = dir('*alpha_15_pylevel_4_kernel_DOG.avi');
cd ..

cd byRegions
%%
for l = 1:length(listing)
    disp(l)
    
    vidObj = VideoReader(listing(l).name);
    k = 1;
    while hasFrame(vidObj)
        F(k).data = readFrame(vidObj);
        k = k+1;
    end
    F(300).data = F(299).data; 
    k = k+1;
    %% Harris
    corners = detectHarrisFeatures(rgb2gray(F(1).data));
    %size(corners.Location,1)
    %imshow(F(1).data), hold on
    %plot(corners)
    %plot(corners.selectStrongest(10));
  
    for rSz = 50 %10*(3:12)
        cornersIn = 0;
        for y = 0:(120-rSz)    
            for x = 0:(180-rSz)
                x1 = x+1; x2 = x+rSz; 
                y1 = y+1; y2 = y+rSz;
                %disp(sprintf('x = %d - %d ; y = %d - %d',x1,x2,y1,y2))
                xIn = (corners.Location(:,1) >= x1) & (corners.Location(:,1) <= x2);
                yIn = (corners.Location(:,2) >= y1) & (corners.Location(:,2) <= y2);
                xyIn = xIn & yIn;
                if cornersIn <= sum(xyIn)
                    cornersIn = sum(xyIn);
                    x1w = x1; x2w = x2;
                    y1w = y1; y2w = y2;
                end             
            end
        end    
        %imshow(F(1).data), hold on
        %plot(corners), rectangle('Position',[x1w y1w rSz rSz],'EdgeColor','r')
        for i = 1:k-1 
            Fc(i).data = F(i).data(y1w:y2w,x1w:x2w,:);
        end
        clearvars -except Fc listing l k rSz
        %% S1: XT-Slice
        yPixel = floor(rSz/2);
        S = zeros(size(Fc(1).data,2),3,k-1,'uint8');
        for i = 1:k-1
           Fci = Fc(i).data;
           S(:,:,i) = squeeze(Fci(yPixel,:,:));   
        end
        S = permute(S,[3 1 2]);
        imwrite(S,strcat(listing(l).name(1:end-4),'_S1.png')) 
        S1 = S;       
        %% S2: 45�-Slice
        P1 = [1 rSz]; P2 = [rSz 1]; 
        x1 = P1(1); y1 = -P1(2);
        x2 = P2(1); y2 = -P2(2);
        x = x1:x2;
        y = round(((y2-y1)/(x2-x1))*(x - x1) + y1);  
        xRange = x2 - x1 + 1;
        S = zeros(xRange,3,k-1,'uint8');
        for i = 1:k-1
           Fci = Fc(i).data;
           for j = 1:length(x)
               S(j,:,i) = Fci(-y(j),x(j),:);
           end
        end   
        S = permute(S,[1 3 2]);
        if (y2-y1)/(x2-x1) > 0
            S = flip(S);  
        end
        imwrite(S,strcat(listing(l).name(1:end-4),'_S2.png')) 
        S2 = S;    
        %% S3: YT-Slice
        xPixel = floor(rSz/2);
        S = zeros(size(Fc(1).data,1),3,k-1,'uint8');
        for i = 1:k-1
            Fci = Fc(i).data;
            S(:,:,i) = squeeze(Fci(:,xPixel,:));   
        end
        S = permute(S,[1 3 2]);
        imwrite(S,strcat(listing(l).name(1:end-4),'_S3.png'))
        S3 = S;  
        %% S4: 135�-Slice
        P1 = [1 1]; P2 = [rSz rSz]; 
        x1 = P1(1); y1 = -P1(2);
        x2 = P2(1); y2 = -P2(2);
        x = x1:x2;
        y = round(((y2-y1)/(x2-x1))*(x - x1) + y1);
        xRange = x2 - x1 + 1;
        S = zeros(xRange,3,k-1,'uint8');
        for i = 1:k-1
           Fci = Fc(i).data;
           for j = 1:length(x)
               S(j,:,i) = Fci(-y(j),x(j),:);
           end
        end   
        S = permute(S,[1 3 2]);
        if (y2-y1)/(x2-x1) > 0
            S = flip(S);  
        end
        imwrite(S,strcat(listing(l).name(1:end-4),'_S4.png'))  
        S4 = S;               
    end
    %%
    %save([listing(l).name(1:end-4),'.mat'],'S1','S2','S3','S4') 
    clearvars -except listing l 
end
cd ..

  
%% Cropping for CNNs
%cd data2
cd results3_accel
clear all, clc

%listing = dir('*.avi');
listing = dir('*fm_15_alpha_15_pylevel_4_kernel_DOG.avi');
%%
L = length(listing);
for l = 1:L
    disp(l)
    load([listing(l).name(1:end-4),'.mat'])  
    S1_1 = S1(1:180,:,:);
    S1_2 = S1(121:end,:,:);
    S2_1 = S2(:,1:180,:);
    S2_2 = S2(:,121:end,:);
    S3_1 = S3(:,1:120,:);
    S3_2 = S3(:,181:end,:);
    S4_1 = S4(:,1:180,:);
    S4_2 = S4(:,121:end,:);
    save([listing(l).name(1:end-4),'_2sq.mat'],...
        'S1_1','S1_2','S2_1','S2_2','S3_1','S3_2','S4_1','S4_2')
end
%[]
cd ..









