SET
%% data2 / results2
clear all, clc
load data2_deepFeatures.mat, dr = 'data2';
load results2_deepFeatures_15.mat, dr = 'results2';
%% Layers & Slices Pairs
dr = 'results3';
layers = {'net1_layerA',...   %1
          'net1_layerB',...   %2
          'net2_layerA',...   %3
          'net2_layerB',...   %4
          'net3_layerA',...   %5
          'net3_layerB'};     %6
PC = [10, 50, 10, 30, 30, 40];      
slicePair = {'S1_1','S1_2';...
             'S2_1','S2_2';...
             'S3_1','S3_2';...
             'S4_1','S4_2'};
mag = 15;
%% For Si_1 and Si_2, net#_layer?
for freq = 11:15
    load(sprintf('results3_deepFeatures_%d_f%d.mat',mag,freq))
    pause(0.1)
    mkdir(sprintf('features_%d_f%d',mag,freq))
    for ly = 1:6
        szCov = eval(['size(V(1).S1_1.',layers{ly},',3);' ]);
        szCovPCA = PC(ly);
        for sN = 1:1 %1:4 
            strng = sprintf('Layer %d - Slice %d',ly,sN);
            disp(strng)             
            CovM1 = zeros(12,szCov,szCov);
            CovM2 = zeros(12,szCov,szCov);
            CovM1_PCA = zeros(12,szCovPCA,szCovPCA);
            CovM2_PCA = zeros(12,szCovPCA,szCovPCA);  
            CovM1_PCAE = zeros(12,szCovPCA,szCovPCA);
            CovM2_PCAE = zeros(12,szCovPCA,szCovPCA);          
            for sP = 1:1 %1:2
                for l = 1:12 
                    eval(['Do = double(V(l).',slicePair{sN,sP},'.',layers{ly},');'])
                    Do = reshape(Do,[],szCov); muDo = mean(Do,1);
                    D = Do - repmat(muDo,size(Do,1),1);
                    eval(sprintf('CovM%d(l,:,:) = (D''*D)/size(Do,1);',sP)) 
                    eval(sprintf('[~,A{l}] = eig(squeeze(CovM%d(l,:,:)));',sP)) %%%% DEBUG
                    eval(sprintf('[Wk,Ak{l}] = eigs(squeeze(CovM%d(l,:,:)),szCovPCA);',sP))
                    eval(sprintf('CovM%d_PCA(l,:,:) = Wk''*squeeze(CovM%d(l,:,:))*Wk;',sP,sP))
                    eval(sprintf('[Wpca,Apca{l}] = eig(squeeze(CovM%d_PCA(l,:,:)));',sP))
                    eval(sprintf('CovM%d_PCAE(l,:,:) = Wpca*logm(Apca{l})*Wpca'';',sP))           
                end
            end
            %eval([layers{ly},'.',slicePair{sN,1},'=','{A,Ak,Apca,CovM1,CovM1_PCA};'])
            %
            %# Descriptor
            descriptorM1 = zeros(12,0.5*(szCovPCA+szCovPCA^2));
            descriptorM2 = zeros(12,0.5*(szCovPCA+szCovPCA^2));
            for l = 1:12
               Tri = tril(ones(szCovPCA,szCovPCA));
               Tri(Tri==0) = NaN;
               covTri = Tri.*squeeze(CovM1_PCAE(l,:,:));
               descriptorM1(l,:) = rmmissing(covTri(:));
               covTri = Tri.*squeeze(CovM2_PCAE(l,:,:));
               descriptorM2(l,:) = rmmissing(covTri(:)); 
            end    
            %{
            % descriptorM1  descriptorM2
            % C1 Si_1       C1 Si_2
            % C2 Si_1       C2 Si_2
            % C3 Si_1       C3 Si_2
            % C4 Si_1       C4 Si_2
            % C5 Si_1       C5 Si_2
            % C6 Si_1       C6 Si_2
            % PK1 Si_1      PK1 Si_2
            % PK2 Si_1      PK2 Si_2
            % PK3 Si_1      PK3 Si_2
            % PK4 Si_1      PK4 Si_2
            % PK5 Si_1      PK5 Si_2
            % PK6 Si_1      PK6 Si_2
            %}
            AllSet = zeros(24,0.5*(szCovPCA+szCovPCA^2));
            for m = 1:2:24
                AllSet(m,:) = descriptorM1(ceil(m/2),:);
                AllSet(m+1,:) = descriptorM2(ceil(m/2),:);
            end
            %{
            % C1 Si_1       
            % C1 Si_2
            % C2 Si_1
            % C2 Si_2
            % C3 Si_1      
            % C3 Si_2
            % C4 Si_1 
            % C4 Si_2
            % C5 Si_1  
            % C5 Si_2
            % C6 Si_1      
            % C6 Si_2
            % PK1 Si_1      
            % PK1 Si_2
            % PK2 Si_1     
            % PK2 Si_2
            % PK3 Si_1     
            % PK3 Si_2
            % PK4 Si_1     
            % PK4 Si_2
            % PK5 Si_1     
            % PK5 Si_2
            % PK6 Si_1     
            % PK6 Si_2
            %}
            AllSetRO = zeros(24,0.5*(szCovPCA+szCovPCA^2));
            idx = cell(12,1);
            c = 1;
            for s = 1:12
                for p = 1:2
                    idx{s}(p) = c;
                    c = c+1;
                end    
            end
            idx = idx([1,12,2,11,3,10,4,9,5,8,6,7]);
            c = 1;
            for s = 1:12
                for p = 1:2
                    AllSetRO(c,:) = AllSet(idx{s}(p),:);
                    c = c+1;            
                end    
            end 
            %{
            % C1 Si_1       
            % C1 Si_2
            % PK6 Si_1     
            % PK6 Si_2
            % C2 Si_1
            % C2 Si_2
            % PK5 Si_1     
            % PK5 Si_2
            % C3 Si_1      
            % C3 Si_2
            % PK4 Si_1     
            % PK4 Si_2
            % C4 Si_1 
            % C4 Si_2
            % PK3 Si_1     
            % PK3 Si_2
            % C5 Si_1  
            % C5 Si_2
            % PK2 Si_1     
            % PK2 Si_2
            % C6 Si_1      
            % C6 Si_2
            % PK1 Si_1      
            % PK1 Si_2
            %}
            K = 12;
            Klines = 24/K;
            c = 0;
            for k = 1:K
                eval(['Test.k',num2str(k),' = zeros(',num2str(Klines),',',num2str(0.5*(szCovPCA+szCovPCA^2)),');'])
                ilinesTest = 1+c : Klines+c;    
                eval(['Test.k',num2str(k),' = AllSetRO(ilinesTest,:);'])
                %   
                eval(['Train.k',num2str(k),' = zeros(',num2str(24-Klines),',',num2str(0.5*(szCovPCA+szCovPCA^2)),');'])
                ilinesTrain = setdiff(1:24,ilinesTest);
                eval(['Train.k',num2str(k),' = AllSetRO(ilinesTrain,:);'])
                c = c + Klines;
            end   
            eval([slicePair{sN,1}(1:2),'.Train = Train;'])
            eval([slicePair{sN,1}(1:2),'.Test = Test;'])
            %} 
            
        clearvars -except mag freq V PC layers ly dr slicePair ...
        szCov szCovPCA S1 S2 S3 S4 ...
        net1_layerA net1_layerB net2_layerA net2_layerB net3_layerA net3_layerB        
        end
    %end    
        eval([dr,'.S1 = S1;'])
        eval([dr,'.S2 = S2;'])
        eval([dr,'.S3 = S3;'])
        eval([dr,'.S4 = S4;'])
        save([dr,'_',layers{ly}],dr)
        disp(['Ok. Moving ',dr,'_',layers{ly},'.mat',' to ',sprintf('features_%d_f%d',mag,freq)])
        movefile([dr,'_',layers{ly},'.mat'],sprintf('features_%d_f%d',mag,freq))
        clearvars -except mag freq V PC layers ly dr slicePair 
    end
%{
%%%%%%%%%%%%%% DEBUGGING
save(['CovEigen_',dr,'_sN1sp1'],'net1_layerA','net1_layerB',...
                      'net2_layerA','net2_layerB',...
                      'net3_layerA','net3_layerB');
%}    
    clear V
end

%% Plotting Eigens 
%{
% Covariances
figure
for sj = 1:12
    Cov = net3_layerB.S1_1{3}(sj,:,:);
    strng = sprintf('%d: %1.4f - %1.4f', sj, min(Cov(:)), max(Cov(:)));
    disp(strng)
    plot(Cov(:),'.')
    pause
end
% Eigenvectors
figure
for sj = 1:12
    eV = net1_layerA.S1_1{1}{sj};
    strng = sprintf('%d: %1.4f - %1.4f', sj, min(eV(:)), max(eV(:)));
    disp(strng)
    plot(eV(:),'.')
    pause
end
% Eigenvalues
figure
subplot(1,6,1), hold on, title('VGG-19 Layer A')
for sj = 1:12
    plot(flip(diag(net1_layerA.S1_1{1}{sj})),'.')
end
xlim([0 64])
subplot(1,6,2), hold on, title('VGG-19 Layer B')
for sj = 1:12
    plot(flip(diag(net1_layerB.S1_1{1}{sj})),'.')
end
xlim([0 128])
subplot(1,6,3), hold on, title('ResNet-101 Layer A')
for sj = 1:12
    plot(flip(diag(net2_layerA.S1_1{1}{sj})),'.')
end
xlim([0 64])
subplot(1,6,4), hold on, title('ResNet-101 Layer B')
for sj = 1:12
    plot(flip(diag(net2_layerB.S1_1{1}{sj})),'.')
end
xlim([0 256])
subplot(1,6,5), hold on, title('InceptResNet-v2 Layer A')
for sj = 1:12
    plot(flip(diag(net3_layerA.S1_1{1}{sj})),'.')
end
xlim([0 64])
subplot(1,6,6), hold on, title('InceptResNet-v2 Layer B')
for sj = 1:12
    plot(flip(diag(net3_layerB.S1_1{1}{sj})),'.')
end
xlim([0 320])
%}
%{
figure

subplot(1,3,1), hold on, box on
title('\textbf{(a)} ResNet-101 Layer A','interpreter','latex')
for sj = 1:12
    plot(flip(diag(net2_layerA.S1_1{1}{sj})),'.')
end
axis([0 64 0 9])
ylabel('$\lambda_i$','interpreter','latex')
xlabel('$i = 1, 2, \cdots, C.$','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','fontsize',12);
grid, grid minor

subplot(1,3,2), hold on, box on
title('\textbf{(b)} ResNet-101 Layer B','interpreter','latex')
for sj = 1:12
    plot(flip(diag(net2_layerB.S1_1{1}{sj})),'.')
end
axis([0 256 0 9])
ylabel('$\lambda_i$','interpreter','latex')
xlabel('$i = 1, 2, \cdots, C.$','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','fontsize',12);
grid, grid minor

subplot(1,3,3), hold on, box on
title('\textbf{(c)} Inception-ResNet-v2 Layer B','interpreter','latex')
for sj = 1:12
    plot(flip(diag(net3_layerB.S1_1{1}{sj})),'.')
end
axis([0 320 0 9])
ylabel('$\lambda_i$','interpreter','latex')
xlabel('$i = 1, 2, \cdots, C.$','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','fontsize',12);
grid, grid minor

savePDFfig('eigenvalues')
%}
%% Comparing Slice Covariances
%{
C = squeeze(net1_layerA.S1_1{5}(2,:,:));

F = ones(10,10).*1e12;
for i = 1:10
    F(i,i) = 1; 
end
squeeze(C.*F);

imagesc(C.*F)

logC = zeros(10,10);
for i = 1:10
    for j = 1:10
        if C(i,j) < 0
            logC(i,j) = -log(-C(i,j));
        elseif C(i,j) > 0    
            logC(i,j) = log(C(i,j));
        end
    end
end

imagesc(logC)
colormap hsv
%}
%% Cummulative Eigenvalues
%{
figure
hold on
for sj = 1:12
    egValSum = cumsum(diag(net3_layerB.S1_1{2}{sj})); 
    plot(egValSum,'.')
    TSum = egValSum(end); TSumP = 0.05*TSum;
    c = 0;
    for i = 1:numel(egValSum)
        if (egValSum(i) >= (TSum - TSumP)) && (c == 0)
            PC(sj) = i;
            disp(i), c = 1;
        end
    end
end
mean(PC)
%                           DATA S1_1   DATA S4_1   RESULTS S1_1    RESULTS S4_1
% net1_layerA => PC: 10     !9.333      9.250           10.33       10.75
% net1_layerB => PC: 50     !50.58      49.17           54.92       52.67
% net2_layerA => PC: 10     !7.250      7.250           8.417       7.750
% net2_layerB => PC: 30     !23.58      25.17           27.50       25.92
% net3_layerA => PC: 30     !23.83      25.33           27.67       23.92
% net3_layerB => PC: 40     !38.00      32.92           34.42       39.58
%}

%% Generating txt's
clear all, clc
layers = {'net1_layerA',...   %1
          'net1_layerB',...   %2
          'net2_layerA',...   %3
          'net2_layerB',...   %4
          'net3_layerA',...   %5
          'net3_layerB'};     %6
dr = {'data2','results3'};
K = 12;
Klines = 24/K; 
labels = {'00','11','00','11',...
          '00','11','00','11',...
          '00','11','00','11'}; 
mag = 15;
%NumSlices = 1;      
%{
% C1 Si_1       
% C1 Si_2
% PK6 Si_1     
% PK6 Si_2
% C2 Si_1
% C2 Si_2
% PK5 Si_1     
% PK5 Si_2
% C3 Si_1      
% C3 Si_2
% PK4 Si_1     
% PK4 Si_2
% C4 Si_1 
% C4 Si_2
% PK3 Si_1     
% PK3 Si_2
% C5 Si_1  
% C5 Si_2
% PK2 Si_1     
% PK2 Si_2
% C6 Si_1      
% C6 Si_2
% PK1 Si_1      
% PK1 Si_2
%}
%%
for freq = 11:15
    disp(sprintf('=> Creating features_%d_f%d',mag,freq))
    cd(sprintf('features_%d_f%d',mag,freq))
    mkdir('S1')
    mkdir('S2')
    mkdir('S4')
    for NumSlices = [1 2 4]
        for ly = 1:6
            for idr = 2:2 %%%%%%% 1:2
                load([dr{idr},'_',layers{ly},'.mat'])
                mkdir([dr{idr},'_',layers{ly}])
                cd([dr{idr},'_',layers{ly}])
                for k = 1:K
                if NumSlices == 1, eval(['concatS_Ktest = [',dr{idr},'.S2.Test.k',num2str(k),'];']), end
                if NumSlices == 2, eval(['concatS_Ktest = [',dr{idr},'.S1.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S3.Test.k',num2str(k),'];']), end
                if NumSlices == 4, eval(['concatS_Ktest = [',dr{idr},'.S1.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S2.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S3.Test.k',num2str(k),...
                                                         ',',dr{idr},'.S4.Test.k',num2str(k),'];']), end                            
                if NumSlices == 1, eval(['concatS_Ktrain = [',dr{idr},'.S2.Train.k',num2str(k),'];']), end
                if NumSlices == 2, eval(['concatS_Ktrain = [',dr{idr},'.S1.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S3.Train.k',num2str(k),'];']), end   
                if NumSlices == 4, eval(['concatS_Ktrain = [',dr{idr},'.S1.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S2.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S3.Train.k',num2str(k),...
                                                          ',',dr{idr},'.S4.Train.k',num2str(k),'];']), end
                     Ktest{k} = concatS_Ktest;
                     Ktrain{k} = concatS_Ktrain;
                end
                clear data2 results2 concatS_Ktest concatS_Ktrain
                for k = 1:K
                    %disp(k)
                   %----------------------------------------------------------------------
                    file = fopen(sprintf('test%02d.txt',k),'w');
                    for s = 1:Klines  
                        sString = '';
                        for x = 1:size(Ktest{k},2)
                            xString = sprintf(' %d:%1.4f',x,Ktest{k}(s,x));
                            sString = [sString,xString];
                        end 
                        sString = [labels{k}(s),sString,'\n'];
                        fprintf(file, sString);       
                    end
                    fclose(file);
                    %----------------------------------------------------------------------
                    file = fopen(sprintf('train%02d.txt',k),'w');
                    idxlb = setdiff(1:K,k); Tlabels = strcat(labels{idxlb});
                    for s = 1:24-Klines 
                        sString = '';
                        for x = 1:size(Ktrain{k},2)
                            xString = sprintf(' %d:%1.4f',x,Ktrain{k}(s,x));
                            sString = [sString,xString];
                        end 
                        sString = [Tlabels(s),sString,'\n'];
                        fprintf(file, sString);
                    end
                    fclose(file);         
                end
                clearvars -except mag freq layers ly dr idr K Klines labels NumSlices
                cd ..
                disp(['Ok. Moving ',dr{idr},'_',layers{ly},' to ','S',num2str(NumSlices)])
                movefile([dr{idr},'_',layers{ly}],['S',num2str(NumSlices)])
            end
        end
    end
    cd ..
end
%% Extract Accuracies
clear all, clc
mag = 15;
for freq = 11:15
    cd(sprintf('features_%d_f%d',mag,freq))
    disp(sprintf('####### DISPLAYING features_%d_f%d #############',mag,freq))   
    for NumSlices = [1 2 4]
        cd(['S',num2str(NumSlices)])
        disp(['%%%%%%%%%% ','S',num2str(NumSlices),' %%%%%%%%%%']) 
        listing = dir('*net3_layerB'); % dir()
        for dr = 1:1 % 3:8
           cd(listing(dr).name)
           disp(listing(dr).name)
           for k = 1:12
               file = fopen(sprintf('k%d.txt',k));
               line = fgetl(file);
               %disp(['k',num2str(k),': ',line])
               numbers = regexp(line,'\d*','Match');
               Acc(k) = str2double(numbers{1});
               disp(Acc(k))
               fclose(file);
           end
           %disp(['MEAN ACC. => ',num2str(mean(Acc),4)]) 
           %disp(num2str(mean(Acc),4)) 
           %disp(' ')
           cd ..
        end
        cd ..
    end
    cd ..
end

%% Boxplots 
N1_LA = zeros(36,1); N1_LB = zeros(36,1);
N2_LA = zeros(36,1); N2_LB = zeros(36,1);
N3_LA = zeros(36,1); N3_LB = zeros(36,1);
N1_LAM = zeros(36,1); N1_LBM = zeros(36,1);
N2_LAM = zeros(36,1); N2_LBM = zeros(36,1);
N3_LAM = zeros(36,1); N3_LBM = zeros(36,1);
D = [N1_LA,N1_LAM,zeros(36,1),N1_LB,N1_LBM,...
     zeros(36,2),...
     N2_LA,N2_LAM,zeros(36,1),N2_LB,N2_LBM,...
     zeros(36,2),...
     N3_LA,N3_LAM,zeros(36,1),N3_LB,N3_LBM];
violinplot(D)
boxplot(D)
violin(D)
distributionPlot(D,'variableWidth','false')

D = [N1_LA,N1_LAM, N1_LB,N1_LBM,...
     N2_LA,N2_LAM, N2_LB,N2_LBM,...
     N3_LA,N3_LAM, N3_LB,N3_LBM]; 
%T = cell2table(cell(12,4), 'VariableNames', {'TP','FP','TN','FN'}, ...
T = cell2table(cell(12,12), 'VariableNames', ...
    {'FP_S1','FP_S2','FP_S4','FN_S1','FN_S2','FN_S4',...
     'TP_S1','TP_S2','TP_S4','TN_S1','TN_S2','TN_S4'}, ... 
    'RowNames',{'N1_LA','N1_LAM', 'N1_LB','N1_LBM',...
                'N2_LA','N2_LAM', 'N2_LB','N2_LBM',...
                'N3_LA','N3_LAM', 'N3_LB','N3_LBM'});
L = {'C';'PK'}; L = repmat(L,18,1);
%% Specificity and Sensitivity
for s = 1:12
    M = cell(36,2);
    for i = 25:36 % 1:36   
        if strcmp(L{i},'C') && (D(i,s) == 100)
            M{i,1} = 'TN'; M{i,2} = 'TN';
        elseif strcmp(L{i},'C') && (D(i,s) == 50)
            M{i,1} = 'TN'; M{i,2} = 'FP';
        elseif strcmp(L{i},'C') && (D(i,s) == 0)
            M{i,1} = 'FP'; M{i,2} = 'FP';

        elseif strcmp(L{i},'PK') && (D(i,s) == 100)
            M{i,1} = 'TP'; M{i,2} = 'TP';
        elseif strcmp(L{i},'PK') && (D(i,s) == 50)
            M{i,1} = 'TP'; M{i,2} = 'FN';
        elseif strcmp(L{i},'PK') && (D(i,s) == 0)
            M{i,1} = 'FN'; M{i,2} = 'FN'; 
        end
    end
    TP = 0; FP = 0; TN = 0; FN = 0;
    for i = 25:36 % 1:36
        if strcmp(M{i,1},'TP') 
            TP = TP + 1;
        elseif strcmp(M{i,1},'FP')
            FP = FP + 1;
        elseif strcmp(M{i,1},'TN')
            TN = TN + 1;
        elseif strcmp(M{i,1},'FN')    
            FN = FN + 1;
        end
        if strcmp(M{i,2},'TP') 
            TP = TP + 1;
        elseif strcmp(M{i,2},'FP')
            FP = FP + 1;
        elseif strcmp(M{i,2},'TN')
            TN = TN + 1;
        elseif strcmp(M{i,2},'FN')    
            FN = FN + 1;
        end     
    end
        %T{s,1} = num2cell(FP);
        %T{s,2} = num2cell(FP);
        T{s,3} = num2cell(FP);
    %T{s,4} = num2cell(FN);
    %T{s,5} = num2cell(FN);
    T{s,6} = num2cell(FN);   
        %T{s,7} = num2cell(TP);
        %T{s,8} = num2cell(TP);
        T{s,9} = num2cell(TP);
    %T{s,10} = num2cell(TN);
    %T{s,11} = num2cell(TN);
    T{s,12} = num2cell(TN);   
        disp(TP+FP+TN+FN)
end
%%
Tm = cell2mat(table2array(T));

FPR = zeros(12,3);
FPR(:,1) = Tm(:,1)./(Tm(:,1)+Tm(:,10));  % For S1
FPR(:,2) = Tm(:,2)./(Tm(:,2)+Tm(:,11));  % For S2
FPR(:,3) = Tm(:,3)./(Tm(:,3)+Tm(:,12));  % For S4
FPR = 100*FPR;
S2 = FPR(:,2); S2(S2==0) = 1; FPR(:,2) = S2;
S4 = FPR(:,3); S4(S4==0) = 1; FPR(:,3) = S4;
FPRf = [FPR([1 2],:); zeros(1,3); FPR([3 4],:);...
       zeros(2,3);...
       FPR([5 6],:); zeros(1,3); FPR([7 8],:);...
       zeros(2,3);...
       FPR([9 10],:); zeros(1,3); FPR([11 12],:)];

FNR = zeros(12,3);
FNR(:,1) = Tm(:,4)./(Tm(:,4)+Tm(:,7));  % For S1
FNR(:,2) = Tm(:,5)./(Tm(:,5)+Tm(:,8));  % For S2
FNR(:,3) = Tm(:,6)./(Tm(:,6)+Tm(:,9));  % For S4
FNR = 100*FNR;
S2 = FNR(:,2); S2(S2==0) = 1; FNR(:,2) = S2;
S4 = FNR(:,3); S4(S4==0) = 1; FNR(:,3) = S4;
FNRf = [FNR([1 2],:); zeros(1,3); FNR([3 4],:);...
       zeros(2,3);...
       FNR([5 6],:); zeros(1,3); FNR([7 8],:);...
       zeros(2,3);...
       FNR([9 10],:); zeros(1,3); FNR([11 12],:)];
   
%%
figure()
fsz = 10;
xlb = {'1S','2S','4S'};
yl = 60;
ylfnr = 45;

subplot(2,3,1), 
   v = [FNR(1,1) FNR(2,1);...
        FNR(1,2) FNR(2,2);...
        FNR(1,3) FNR(2,3)];
B1 = bar(v); ylim([0 ylfnr]) 
set(B1,'LineStyle','none');
titlelb = '\textbf{(a)} \begin{tabular}{c} VGG-19 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FNR','interpreter','latex')

subplot(2,3,4),
   v = [FPR(1,1) FPR(2,1);...
        FPR(1,2) FPR(2,2);...
        FPR(1,3) FPR(2,3)];
B2 = bar(v); ylim([0 yl])
set(B2,'LineStyle','none');
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FPR','interpreter','latex')


subplot(2,3,2), 
   v = [FNR(5,1) FNR(6,1);...
        FNR(5,2) FNR(6,2);...
        FNR(5,3) FNR(6,3)];
B1 = bar(v); ylim([0 ylfnr]) 
set(B1,'LineStyle','none');
titlelb = '\textbf{(b)} \begin{tabular}{c} ResNet-101 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FNR','interpreter','latex')

subplot(2,3,5), 
   v = [FPR(5,1) FPR(6,1);...
        FPR(5,2) FPR(6,2);...
        FPR(5,3) FPR(6,3)];
B2 = bar(v); ylim([0 yl]) 
set(B2,'LineStyle','none');
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FPR','interpreter','latex')


subplot(2,3,3), 
   v = [FNR(9,1) FNR(10,1);...
        FNR(9,2) FNR(10,2);...
        FNR(9,3) FNR(10,3)];
B1 = bar(v); ylim([0 ylfnr]) 
set(B1,'LineStyle','none');
titlelb = '\textbf{(c)} \begin{tabular}{c} Inception-ResNet-v2 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FNR','interpreter','latex')

subplot(2,3,6), 
   v = [FPR(9,1) FPR(10,1);...
        FPR(9,2) FPR(10,2);...
        FPR(9,3) FPR(10,3)];
B2 = bar(v); ylim([0 yl]) 
set(B2,'LineStyle','none');
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FPR','interpreter','latex')

legend('Standard','Magnified','interpreter','latex','fontsize',fsz)


%%
figure()

subplot(2,1,1)
B = bar(FPRf,1.5); set(B,'LineStyle','none');
ylim([0 75]), grid on
set(gca,'ytick',0:10:70); ylabel('FPR','interpreter','latex')
labels = {'','','','','','','',...
             '','','','','','','',...
             '','','','',''};
set(gca,'xtick',1:19,'XTickLabel',labels);
set(gca,'TickLabelInterpreter','latex','fontsize',12);
set(gca,'XColor','k')
legend(B,{'1 Slice','2 Slices','4 Slices'},'interpreter','latex','fontsize',8)

subplot(2,1,2)
B = bar(FNRf,1.5); set(B,'LineStyle','none');
ylim([0 50]), grid on
set(gca,'ytick',0:10:40); ylabel('FNR','interpreter','latex')
labels = {'Std','Mag','','Std','Mag','','',...
             'Std','Mag','','Std','Mag','','',...
             'Std','Mag','','Std','Mag'};
set(gca,'xtick',1:19,'XTickLabel',labels);
set(gca,'TickLabelInterpreter','latex','fontsize',12);
set(gca,'XColor','k')
legend(B,{'1 Slice','2 Slices','4 Slices'},'interpreter','latex','fontsize',8)

pos = get(gca,'position')
set(gca,'position',[pos(1) pos(2)+0.1175 pos(3) pos(4)])
pos = [0.13,0.082,0.775,0.075];
str0 = '\,\,Layer A\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,Layer B';
str = [str0,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',str0];
str = [str,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',str0];
annotation('textbox',pos,'String',str,'FontSize',12,'Interpreter','latex','LineStyle','none')

str0 = 'VVG-19'; str1 = 'ResNet-101'; str2 = 'Inception-ResNet-v2';
str = ['\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',...
    str0,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',...
    str1,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',...
    str2];
pos = [0.13,0.01,0.775,0.075];
annotation('textbox',pos,'String',str,'FontSize',12,'Interpreter','latex','LineStyle','none')

%%
%savePDFfig('acc')

figure()
fsz = 11;
fsz2 = 7;
ypos = 3;
s1p1 = 0.65; s1p2 = 1.05;
s2p1 = s1p1+1; s2p2 = s1p2+1;
s4p1 = s1p1+2; s4p2 = s1p2+2;
xlb = {'1S','2S','4S'};

subplot(2,3,1), 
v = [66.7 75; 70.8 83.3; 100 100];
B = bar(v); % VGG-19 Layer A: S1 S2 S4
%B(1).FaceColor = [0.3,0.75,0.93]; 
%B(2).FaceColor = [0.9,0.6,0.4]; 
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(a)} \begin{tabular}{c} VGG-19 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2-0.05,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

subplot(2,3,4), 
v = [45.8 50; 100 100; 91.7 100];
B = bar(v); % VGG-19 Layer B: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(b)} \begin{tabular}{c} VGG-19 \\ Layer B \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)


subplot(2,3,2),
v = [75 54.2; 79.2 91.7; 87.5 100];
B = bar(v); % ResNet-101 Layer A: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(c)} \begin{tabular}{c} ResNet-101 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1+0.1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2-0.05,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

subplot(2,3,5),
v = [45.8 79.2; 91.7 95.8; 91.7 100];
B = bar(v); % ResNet-101 Layer B: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(d)} \begin{tabular}{c} ResNet-101 \\ Layer B \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)


subplot(2,3,3),
v = [58.3 66.7; 100 100; 100 100];
B = bar(v); % Inception-ResNet-v2 Layer A: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(e)} \begin{tabular}{c} Inception-ResNet-v2 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

subplot(2,3,6), 
v = [54.2 62.5; 87.5 91.7; 91.7 83.3];
B = bar(v); % Inception-ResNet-v2 Layer B: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(f)} \begin{tabular}{c} Inception-ResNet-v2 \\ Layer B \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

legend('Standard','Magnified','interpreter','latex','fontsize',10)

%% LibSVM
cd libsvm-3.23w\tools
tic
parfor k = 1:12
   disp(sprintf('Starting k%02d',k))
   cmd0 = 'python easy.py ';
   cmd1 = ['..\..\results2_features\exp2\data2_net1_layerA\',sprintf('train%02d.txt ',k)];
   cmd2 = ['..\..\results2_features\exp2\data2_net1_layerA\',sprintf('test%02d.txt ',k)];
   cmd3 = ['> ..\..\results2_features\exp2\data2_net1_layerA\',sprintf('k%02d.txt',k)]; 
   system([cmd0,cmd1,cmd2,cmd3]); 
   disp(sprintf('Ok k%02d',k))
end
toc
cd ..\..
%Elapsed time is 75.112836 seconds.


%%
% cd libsvm-3.23w\tools
% python easy.py ..\..\results2_features\data2_net1_layerA\train01.txt ..\..\results2_features\data2_net1_layerA\test01.txt > ..\..\results2_features\data2_net1_layerA\k1.txt
% cd ..\..
% 
% imshow(squeeze(CovM1(12,:,:)),[])
% imshow(squeeze(CovM2(1,:,:)),[])
% 
% plotCov = permute(CovM1,[2 3 1]);
% grid = imtile(mat2gray(plotCov),'GridSize',[3 4]);
% imshow(grid,[])
% imshow(squeeze(CovM1(1,:,:)))
% colormap hsv
% colormap jet
% 
% A = V(1).S1_1.net1_layerA(:,:,1);
% B = V(1).S1_1.net1_layerA(:,:,2);
% Cov = cov(A,B); Cov = Cov(2);

