%test = [1 2 3; 4 5 6; 7 8 9]
%test = test'
%test(:)

%% Originals Gradient/Scaled Slices
clear all
cd data
listing = dir('*_S1.png');
for l = 1:length(listing)
    S = imread(listing(l).name);
    S(300,:,:) = S(299,:,:);
    Sg = rgb2gray(S);
    [Gx,Gy] = imgradientxy(Sg,'sobel');
    [Gmag,~] = imgradient(Gx,Gy);
    [Gxx,Gyy] = gradient(Gmag);
    [Gmag2,~] = imgradient(Gxx,Gyy);
    save(strcat(listing(l).name(1:end-4),'.mat'),'Sg','Gx','Gy','Gmag','Gxx','Gyy','Gmag2') 
    imwrite(uint8(rNorm(Gx,0,255)),strcat(listing(l).name(1:end-4),'_Gx.png'))
    imwrite(uint8(rNorm(Gy,0,255)),strcat(listing(l).name(1:end-4),'_Gy.png'))
    imwrite(uint8(rNorm(Gmag,0,255)),strcat(listing(l).name(1:end-4),'_Gmag.png'))
    
    %{ 
    Sgr = imresize(Sg,1/3);
    Gxr = imresize(Gx,1/3);
    Gyr = imresize(Gy,1/3);
    Gmagr = imresize(Gmag,1/3);
    imwrite(Sgr,strcat(listing(l).name(1:end-4),'_R.png'))        
    imwrite(uint8(rNorm(Gxr,0,255)),strcat(listing(l).name(1:end-4),'_Gx_R.png'))    
    imwrite(uint8(rNorm(Gyr,0,255)),strcat(listing(l).name(1:end-4),'_Gy_R.png'))
    imwrite(uint8(rNorm(Gmagr,0,255)),strcat(listing(l).name(1:end-4),'_Gmag_R.png'))     
    %}
end
l2 = dir('*_S2.png');
l3 = dir('*_S3.png');
l4 = dir('*_S4.png');
listing = [l2;l3;l4];
for l = 1:length(listing)
    S = imread(listing(l).name);
    S(:,300,:) = S(:,299,:);
    Sg = rgb2gray(S);
    [Gx,Gy] = imgradientxy(Sg,'sobel');
    [Gmag,~] = imgradient(Gx,Gy);
    [Gxx,Gyy] = gradient(Gmag);
    [Gmag2,~] = imgradient(Gxx,Gyy);
    save(strcat(listing(l).name(1:end-4),'.mat'),'Sg','Gx','Gy','Gmag','Gxx','Gyy','Gmag2') 
    imwrite(uint8(rNorm(Gx,0,255)),strcat(listing(l).name(1:end-4),'_Gx.png'))
    imwrite(uint8(rNorm(Gy,0,255)),strcat(listing(l).name(1:end-4),'_Gy.png'))
    imwrite(uint8(rNorm(Gmag,0,255)),strcat(listing(l).name(1:end-4),'_Gmag.png'))

    %{ 
    Sgr = imresize(Sg,1/3);
    Gxr = imresize(Gx,1/3);
    Gyr = imresize(Gy,1/3);
    Gmagr = imresize(Gmag,1/3);
    imwrite(Sgr,strcat(listing(l).name(1:end-4),'_R.png'))        
    imwrite(uint8(rNorm(Gxr,0,255)),strcat(listing(l).name(1:end-4),'_Gx_R.png'))    
    imwrite(uint8(rNorm(Gyr,0,255)),strcat(listing(l).name(1:end-4),'_Gy_R.png'))
    imwrite(uint8(rNorm(Gmagr,0,255)),strcat(listing(l).name(1:end-4),'_Gmag_R.png'))     
    %}    
end
cd ..

%% Results Gradient/Scaled Slices
clear all
cd results_accel
folders = {'C1','C2','C3','C4','C5','C6',...
           'PK1','PK2','PK3','PK4','PK5','PK6'};
for f = 1:length(folders)
    disp(f)
    cd(folders{f}) 
    listing = dir('*_S1.png');
    for l = 1:length(listing)
        S = imread(listing(l).name);
        S(300,:,:) = S(299,:,:);
        Sg = rgb2gray(S);
        [Gx,Gy] = imgradientxy(Sg,'sobel');
        [Gmag,~] = imgradient(Gx,Gy);
        [Gxx,Gyy] = gradient(Gmag);
        [Gmag2,~] = imgradient(Gxx,Gyy);
        save(strcat(listing(l).name(1:end-4),'.mat'),'Sg','Gx','Gy','Gmag','Gxx','Gyy','Gmag2') 
        imwrite(uint8(rNorm(Gx,0,255)),strcat(listing(l).name(1:end-4),'_Gx.png'))
        imwrite(uint8(rNorm(Gy,0,255)),strcat(listing(l).name(1:end-4),'_Gy.png'))
        imwrite(uint8(rNorm(Gmag,0,255)),strcat(listing(l).name(1:end-4),'_Gmag.png'))

        %{ 
        Sgr = imresize(Sg,1/3);
        Gxr = imresize(Gx,1/3);
        Gyr = imresize(Gy,1/3);
        Gmagr = imresize(Gmag,1/3);
        imwrite(Sgr,strcat(listing(l).name(1:end-4),'_R.png'))        
        imwrite(uint8(rNorm(Gxr,0,255)),strcat(listing(l).name(1:end-4),'_Gx_R.png'))    
        imwrite(uint8(rNorm(Gyr,0,255)),strcat(listing(l).name(1:end-4),'_Gy_R.png'))
        imwrite(uint8(rNorm(Gmagr,0,255)),strcat(listing(l).name(1:end-4),'_Gmag_R.png'))     
        %}
    end
    l2 = dir('*_S2.png');
    l3 = dir('*_S3.png');
    l4 = dir('*_S4.png');
    listing = [l2;l3;l4];
    for l = 1:length(listing)
        S = imread(listing(l).name);
        S(:,300,:) = S(:,299,:);
        Sg = rgb2gray(S);
        [Gx,Gy] = imgradientxy(Sg,'sobel');
        [Gmag,~] = imgradient(Gx,Gy);
        [Gxx,Gyy] = gradient(Gmag);
        [Gmag2,~] = imgradient(Gxx,Gyy);
        save(strcat(listing(l).name(1:end-4),'.mat'),'Sg','Gx','Gy','Gmag','Gxx','Gyy','Gmag2') 
        imwrite(uint8(rNorm(Gx,0,255)),strcat(listing(l).name(1:end-4),'_Gx.png'))
        imwrite(uint8(rNorm(Gy,0,255)),strcat(listing(l).name(1:end-4),'_Gy.png'))
        imwrite(uint8(rNorm(Gmag,0,255)),strcat(listing(l).name(1:end-4),'_Gmag.png'))

        %{ 
        Sgr = imresize(Sg,1/3);
        Gxr = imresize(Gx,1/3);
        Gyr = imresize(Gy,1/3);
        Gmagr = imresize(Gmag,1/3);
        imwrite(Sgr,strcat(listing(l).name(1:end-4),'_R.png'))        
        imwrite(uint8(rNorm(Gxr,0,255)),strcat(listing(l).name(1:end-4),'_Gx_R.png'))    
        imwrite(uint8(rNorm(Gyr,0,255)),strcat(listing(l).name(1:end-4),'_Gy_R.png'))
        imwrite(uint8(rNorm(Gmagr,0,255)),strcat(listing(l).name(1:end-4),'_Gmag_R.png'))     
        %}
    end
    cd ..
end       
cd ..




