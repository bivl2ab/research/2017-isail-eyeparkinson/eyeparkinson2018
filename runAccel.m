%% Setup
addpath(genpath('accel'))
addpath(genpath('data2'))
addpath(genpath('results2_accel'))
rD = 'results3_accel';
dD = 'data2';

%%
cd data2\
listing = dir('*.avi');
cd ..

%% Running 3
freq = [11 12 13 14 15];%[4 5 6 7 8 9 10]
for ifreq = 1:5
    parfor ivid = 1:12
        fprintf('Processing freq:%d vid:%d \n',freq(ifreq),ivid)
        vid = listing(ivid).name(1:end-4);
        [vid_in,params] = setparameters(vid,'.avi',dD,rD,freq(ifreq),15,'DOG');
        motionamp(vid_in,params); 
        pause(0.1) 
    end
end


%% Running 2
alpha = 5;%[10 15 20 25 30];
for ialpha = 1:1
    parfor ivid = 1:12
        fprintf('Processing alpha:%d vid:%d \n',alpha(ialpha),ivid)
        vid = listing(ivid).name(1:end-4);
        [vid_in,params] = setparameters(vid,'.avi',dD,rD,5.7,alpha(ialpha),'DOG');
        motionamp(vid_in,params); 
        pause(0.1) 
    end
end
   


%% Running PK
fq = [5 5.2 5.4 5.6 5.8 6];
alpha = [10 15 20 30];
timerDOG = zeros(144,1);
timerINT = zeros(144,1);
c = 1;
for ifq = 1:6
    for ialpha = 1:3
        parfor ivid = 1:6
            fprintf('Processing fq:%1.1f alpha:%d vid:%d \n',fq(ifq),alpha(ialpha),ivid)
            vid = sprintf('PK%d',ivid);
            %tic
            [vid_in,params] = setparameters(vid,'.avi',dD,rD,fq(ifq),alpha(ialpha),'DOG');
            motionamp(vid_in,params); 
            %timerDOG(c) = toc;
            pause(0.1)
            %tic
            [vid_in,params] = setparameters(vid,'.avi',dD,rD,fq(ifq),alpha(ialpha),'INT');
            motionamp(vid_in,params);         
            %timerINT(c) = toc;
            pause(0.1)
            %c = c+1;
        end
    end
end   

%% Running C
fq = [5.6 5.8];
alpha = [10 15 20];
timerDOG = zeros(144,1);
timerINT = zeros(144,1);
c = 1;
for ifq = 1:6
    for ialpha = 1:3
        parfor ivid = 1:6
            fprintf('Processing fq:%1.1f alpha:%d vid:%d \n',fq(ifq),alpha(ialpha),ivid)
            vid = sprintf('C%d',ivid);
            %tic
            [vid_in,params] = setparameters(vid,'.avi',dD,rD,fq(ifq),alpha(ialpha),'DOG');
            motionamp(vid_in,params); 
            %timerDOG(c) = toc;
            pause(0.1)
            %tic
            [vid_in,params] = setparameters(vid,'.avi',dD,rD,fq(ifq),alpha(ialpha),'INT');
            motionamp(vid_in,params);         
            %timerINT(c) = toc;
            pause(0.1)
            %c = c+1;
        end
    end
end   

%% Examples
%%% Synehtic ball video %%%%%%%%%%%%%%%%%%%%%%%%%%
[vid_in,params] = setparameters('syn_ball', '.avi','data','results_accel',10/3,5,'DOG');
motionamp(vid_in,params);

%%% Cat toy video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('cat_toy','.mp4','data','results_accel',4,8,'INT');
motionamp(vid_in,params);

%%% Gun shot video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('gun_shot','.mp4','data','results_accel',8,8,'DOG');
motionamp(vid_in,params);

%%% Parkinson I video %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('parkinson1','.mp4','data','results_accel',3,8,'INT');
motionamp(vid_in,params);

%%% Parkinson II video %%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('parkinson2','.mp4','data','results_accel',3,8,'INT');
motionamp(vid_in,params);

%%% Bottle video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('bottle_moving','.mp4','data','results_accel',4,8,'INT');
motionamp(vid_in,params);

%%% Eye video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('eye_raw','.mp4','data','results_accel',2.5,15,'DOG');
motionamp(vid_in,params);


